import numpy as np

class TicTacToeGameController:
    def __init__(self):
        self.board = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
        self.piecesPlaced = 0

    def setPiece(self, row, col, piece):
        if self.board[row][col] == 0:
            self.board[row][col] = piece
            self.piecesPlaced += 1
            return True
        return False
    
    def check_rows(self):
        # Check if there is a winner in the rows
        if self.board[0][0] == self.board[0][1] == self.board[0][2] != 0:
            return self.board[0][0]
        elif self.board[1][0] == self.board[1][1] == self.board[1][2] != 0:
            return self.board[1][0]
        elif self.board[2][0] == self.board[2][1] == self.board[2][2] != 0:
            return self.board[2][0]


    def check_cols(self):
        # Check if there is a winner in the cols
        if self.board[0][0] == self.board[1][0] == self.board[2][0] != 0:
            return self.board[0][0]
        elif self.board[0][1] == self.board[1][1] == self.board[2][1] != 0:
            return self.board[0][1]
        elif self.board[0][2] == self.board[1][2] == self.board[2][2] != 0:
            return self.board[0][2]

    def check_diagonals(self):
        # Check if there is a winner in the diagonals
        if self.board[0][0] == self.board[1][1] == self.board[2][2] != 0:
            return self.board[0][0]
        elif self.board[0][2] == self.board[1][1] == self.board[2][0] != 0:
            return self.board[0][2]

    def checkVictory(self):
        # Check if there is a winner
        if self.check_rows() or self.check_cols() or self.check_diagonals():
            return True
        else:
            return None

    
    def end(self):
        if(self.piecesPlaced == 9):
            return True
        return False
    
    def toString(self):
        return self.board