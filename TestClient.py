import TicTacToeGame

game = TicTacToeGame.TicTacToeGameController()

playerTurn = 1
while (game.checkVictory() == None) and (game.end() == False):
    print(game.toString())
    x = int(input(f"column player {playerTurn}: "))
    y = int(input(f"row player {playerTurn}: "))
    if game.setPiece(y-1, x-1, playerTurn) == False:
        print("Invalid move")
        continue
    else:
        playerTurn = 2 if playerTurn == 1 else 1

if game.checkVictory() == None:
    print("Draw")
else:
    print(f"player: {game.checkVictory()} won")